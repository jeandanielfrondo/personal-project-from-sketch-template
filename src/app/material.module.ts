import { NgModule } from '@angular/core';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';

@NgModule({
  imports: [MatMenuModule, MatIconModule, MatToolbarModule],
  exports: [MatMenuModule, MatIconModule, MatToolbarModule],
})
export class MaterialModule {}
